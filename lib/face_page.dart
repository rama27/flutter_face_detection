import 'dart:io';
import 'dart:ui' as ui;

import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class FacePage extends StatefulWidget {
  @override
  _FacePageState createState() => _FacePageState();
}

class _FacePageState extends State<FacePage> {
  File _imageFile;
  List<Face> _faces;
  ui.Image _image;

  void _getImageAndDetectFaces() async{
    final imageFile = await ImagePicker.pickImage(
      source: ImageSource.camera
    );    
    final image = FirebaseVisionImage.fromFile(imageFile);
    final faceDetector = FirebaseVision.instance.faceDetector(
      FaceDetectorOptions(
        mode: FaceDetectorMode.accurate,
      )
    );
    final faces = await faceDetector.processImage(image);
    if(mounted){
      setState(() {
        _imageFile = imageFile;
        _faces = faces;
      });
    }
   
  }

// @override
//   void initState() {
//     FirebaseApp.initializeApp(this);
//     super.initState();
//   }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Face Detector'),
      ),
      body : ImagesAndFaces(imageFile: _imageFile, faces: _faces,),
      floatingActionButton: FloatingActionButton(
        onPressed: () { _getImageAndDetectFaces(); },
        tooltip: 'Pick an image',
        child: Icon(Icons.add_a_photo),
      ),
    );
  }
}


class ImagesAndFaces extends StatelessWidget {
  final File imageFile;
  final List<Face> faces;
  ImagesAndFaces({@required this.imageFile, @required this.faces});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
         Flexible(
           flex : 2,          
           child: Container(
             constraints: BoxConstraints.expand(),
             child: imageFile != null ?Image.file(
               imageFile,
               fit : BoxFit.cover
             ) : Container(),
           ), 
         ),
         Flexible(
           flex: 1,
           child: ListView(
             children: faces.map<Widget>((f) => FaceCoordinates(f)).toList(),
           ),
         )
      ],
    );
  }
}

class FaceCoordinates extends StatelessWidget {
  FaceCoordinates(this.face);
  final Face face;
  @override
  Widget build(BuildContext context) {
    final pos = face.boundingBox;
    return ListTile(
      title: Text('(${pos.top}, ${pos.left}), (${pos.bottom}), ${pos.right})'),
    );
  }  
}